<div align="center" style="padding-top: 48px; padding-bottom: 16px;">
    <img src="docs/images/logo-with-background.png" alt="Burst Logo" width="64" />
    <p><b>Burst</b></p>
</div>

Burst is under active development. Check out the [Roadmap](https://sharing.acreom.com/d/20bb1ea1-be4c-4e84-94c5-2c3bf7d20925)!
