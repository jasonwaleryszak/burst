import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.System;
import Toybox.Application;
import Toybox.Lang;

class PomodoroView extends WatchUi.View {
    hidden var durationText;
    hidden var timeText;

    function initialize() {
        View.initialize();
    }

    function onShow() {
        timeText = new WatchUi.Text({
            :text=>"time",
            :color=>Graphics.COLOR_WHITE,
            :font=>Graphics.FONT_TINY,
            :locX=>WatchUi.LAYOUT_HALIGN_CENTER,
            :locY=>WatchUi.LAYOUT_VALIGN_TOP
        });

        durationText = new WatchUi.Text({
            :text=>"duration",
            :color=>Graphics.COLOR_WHITE,
            :font=>Graphics.FONT_NUMBER_MEDIUM,
            :locX=>WatchUi.LAYOUT_HALIGN_CENTER,
            :locY=>WatchUi.LAYOUT_VALIGN_CENTER
        });
    }

    function onUpdate(dc) {
        var app = Application.getApp() as PomodoroApp;
        var durationSeconds = app.cycleRemainingDurationSeconds;
        
        var secondsRemaining = durationSeconds % 60;
        var minutesRemaining = (durationSeconds / 60).abs();

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        dc.clear();
        
        durationText.setText(Lang.format("$1$:$2$", [minutesRemaining.format("%02d"), secondsRemaining.format("%02d")]));

        if (app.currentState == $.POST_WORK || app.currentState == $.BREAK) {
            durationText.setColor(0x0652DD);
        } else {
            durationText.setColor(Graphics.COLOR_WHITE);
        }

        durationText.draw(dc);

        var currentTime = System.getClockTime();
        timeText.setText(Lang.format("$1$:$2$", [currentTime.hour.format("%02d"), currentTime.min.format("%02d")]));
        timeText.draw(dc);
    }

}
