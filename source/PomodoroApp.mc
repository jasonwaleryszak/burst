import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
import Toybox.Time;
import Toybox.Timer;
import Toybox.System;
import Toybox.Attention;

public enum State {
    INITIAL,
    WORK,
    POST_WORK,
    BREAK,
    POST_BREAK
}

const StateByIndex = [$.INITIAL, $.WORK, $.POST_WORK, $.BREAK, $.POST_BREAK];

class PomodoroApp extends Application.AppBase {
    var WORK_DURATION_MINUTES;
    var WORK_DURATION_SECONDS;
    var BREAK_DURATION_MINUTES;
    var BREAK_DURATION_SECONDS;

    var pomodoroTimer as Timer.Timer;
    var cycleEndTime as Time.Moment;
    var cycleRemainingDurationSeconds as Integer;
    var currentState as State;
    var isPaused as Boolean;
    var clockTimer as ClockTimer;

    function initialize() {
        AppBase.initialize();

        pomodoroTimer = new Timer.Timer();
        clockTimer = new ClockTimer();

        refreshCycleDurations();
    }

    function onStart(state) {
        System.println("onStart()");
        restoreState();

        clockTimer.start();
    }

    function restoreState() {
        var cycleEndTimeSeconds = Application.Storage.getValue("cycleEndTimeSeconds");

        if (cycleEndTimeSeconds != null) {
            cycleEndTime = new Time.Moment(cycleEndTimeSeconds);
        }

        cycleRemainingDurationSeconds = Application.Storage.getValue("cycleRemainingDurationSeconds");

        if (cycleRemainingDurationSeconds == null) {
            cycleRemainingDurationSeconds = WORK_DURATION_SECONDS;
        }

        var currentStateIndex = Application.Storage.getValue("currentState");

        if (currentStateIndex == null) {
            currentState = $.INITIAL;
        } else {
            currentState = StateByIndex[currentStateIndex];
        }

        isPaused = Application.Storage.getValue("isPaused");

        if (isPaused == null) {
            isPaused = false;
        }

        if (currentState == $.INITIAL || isPaused) {
            return;
        }

        var currentTime = Time.now();
        cycleRemainingDurationSeconds = cycleEndTime.subtract(currentTime).value();

        if (currentTime.greaterThan(cycleEndTime)) {
            if (currentState == $.WORK || currentState == $.POST_WORK) {
                currentState = $.POST_WORK;
                cycleRemainingDurationSeconds = BREAK_DURATION_SECONDS;
            } else if (currentState == $.BREAK || currentState == $.POST_BREAK) {
                currentState = $.POST_BREAK;
                cycleRemainingDurationSeconds = WORK_DURATION_SECONDS;
            }
            alertCycleStateExit();
        } else {
            pomodoroTimer.start(method(:onTimerTick), 1000, true);
        }
    }

    function onStop(state) {
        System.println("onStop()");
        saveState();

        pomodoroTimer.stop();
        clockTimer.stop();
    }

    function saveState() {
        Application.Storage.setValue("cycleEndTimeSeconds", cycleEndTime.value());
        Application.Storage.setValue("cycleRemainingDurationSeconds", cycleRemainingDurationSeconds);
        Application.Storage.setValue("currentState", currentState);
        Application.Storage.setValue("isPaused", isPaused);
    }

    function onSettingsChanged() {
        refreshCycleDurations();
    }
 
    function refreshCycleDurations() {
        WORK_DURATION_MINUTES = Application.Properties.getValue("work_cycle_duration");
        WORK_DURATION_SECONDS = WORK_DURATION_MINUTES * 60; // 60
        BREAK_DURATION_MINUTES = Application.Properties.getValue("break_cycle_duration");
        BREAK_DURATION_SECONDS = BREAK_DURATION_MINUTES * 60; // 60
    }

    function onTimerTick() {
        var currentTime = Time.now();
        cycleRemainingDurationSeconds = cycleEndTime.subtract(currentTime).value();

        if (currentTime.greaterThan(cycleEndTime)) {
            if (currentState == $.WORK) {
                pomodoroTimer.stop();
                currentState = $.POST_WORK;
                cycleRemainingDurationSeconds = BREAK_DURATION_SECONDS;
            } else if (currentState == $.BREAK) {
                pomodoroTimer.stop();
                currentState = $.POST_BREAK;
                cycleRemainingDurationSeconds = WORK_DURATION_SECONDS;
            }

            alertCycleStateExit();
        }

        WatchUi.requestUpdate();
    }

    function getInitialView() as Array<Views or InputDelegates>? {
        return [ new PomodoroView(), new PomodoroDelegate() ] as Array<Views or InputDelegates>;
    }

    function alertCycleStateEnter() as Void {
        Attention.vibrate([
            new Attention.VibeProfile(75, 200)
        ]);
    }

    function alertCycleStateExit() as Void {
        Attention.vibrate([
            new Attention.VibeProfile(75, 200),
            new Attention.VibeProfile(0, 100),
            new Attention.VibeProfile(75, 200),
            new Attention.VibeProfile(0, 400),
            new Attention.VibeProfile(75, 200),
            new Attention.VibeProfile(0, 100),
            new Attention.VibeProfile(75, 200)
        ]);
    }

    function alertCyclePauseStateChange() as Void {
        Attention.vibrate([
            new Attention.VibeProfile(75, 200)
        ]);
    }

}

function getApp() as PomodoroApp {
    return Application.getApp() as PomodoroApp;
}