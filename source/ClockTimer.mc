import Toybox.Timer;
import Toybox.System;
import Toybox.WatchUi;

/**
 * Represents a clock timer.
 */
class ClockTimer {
    private var timer as Timer.Timer;

    public function initialize( ) {
        timer = new Timer.Timer();
    }

    // Starts the timer aligned to the next minute.
    public function start() {
        alignTimer();
    }

    // Stops the timer.
    public function stop() {
        timer.stop();
    }

    // Aligns the timer to the next minute.
    function alignTimer() {
        var currentTime = System.getClockTime();
        var nextTick = (60 - currentTime.sec) * 1000;
        timer.start(method(:alignTimerCallback), nextTick, false);
    }


    // Callback for alignTimer, starts the timer that ticks every 60 seconds.
    function alignTimerCallback() {
        timer.stop();
        WatchUi.requestUpdate();
        timer.start(method(:onTimerTick), 60000, true);
    }

    // Callback for the timer, requests an update to the Watch UI.
    function onTimerTick() {
        WatchUi.requestUpdate();
    }
}