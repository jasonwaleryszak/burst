import Toybox.Lang;
import Toybox.WatchUi;
import Toybox.Time;

class PomodoroDelegate extends WatchUi.BehaviorDelegate {

    function initialize() {
        BehaviorDelegate.initialize();
    }

    public function onSelect() as Boolean {
        var app = Application.getApp() as PomodoroApp;

        // if currently paused, unpause.
        if (app.isPaused) {
            app.cycleEndTime = Time.now().add(new Time.Duration(app.cycleRemainingDurationSeconds));
            app.isPaused = false;
            app.alertCyclePauseStateChange();
            // unsuspend the timer for the current cycle.
            app.pomodoroTimer.start(app.method(:onTimerTick), 1000, true);
            return true;
        }

        // if not paused and in the middle of a cycle, pause.
        if (app.currentState == $.WORK || app.currentState == $.BREAK) {
            // suspend the timer for the current cycle.
            app.pomodoroTimer.stop();
            app.isPaused = true;
            app.alertCyclePauseStateChange();
            return true;
        }

        // if the app has just been launched of post-cycle, start the next cycle.
        if (app.currentState == $.INITIAL || app.currentState == $.POST_BREAK) {
            app.currentState = $.WORK;
            app.cycleEndTime = Time.now().add(new Time.Duration(app.WORK_DURATION_SECONDS));
            app.cycleRemainingDurationSeconds = app.WORK_DURATION_SECONDS;
        } else if (app.currentState == $.POST_WORK) {
            app.currentState = $.BREAK;
            app.cycleEndTime = Time.now().add(new Time.Duration(app.BREAK_DURATION_SECONDS));
            app.cycleRemainingDurationSeconds = app.BREAK_DURATION_SECONDS;
        }

        app.alertCycleStateEnter();

        // start the timer for the new cycle.
        app.pomodoroTimer.start(app.method(:onTimerTick), 1000, true);

        return true;
    }

    public function onPreviousPage() as Boolean {
        var app = Application.getApp() as PomodoroApp;

        app.pomodoroTimer.stop();

        app.currentState = $.INITIAL;
        app.cycleRemainingDurationSeconds = app.WORK_DURATION_SECONDS;
        app.isPaused = false;

        WatchUi.requestUpdate();

        return true;
    }
}