// import Toybox.Timer;
// import Toybox.Lang;
// import Toybox.Time;

// public enum PomodoroState {
//     WORK,
//     POST_WORK,
//     BREAK,
//     POST_BREAK
// }

// const PomodoroStateByIndex = [$.WORK, $.POST_WORK, $.BREAK, $.POST_BREAK];

// class PomodoroDuration {
//     private var minutes as Lang.Number;
//     private var seconds as Lang.Number;

//     public function initialize(minutes as Lang.Number, seconds as Lang.Number) {
//         self.minutes = minutes;
//         self.seconds = seconds;
//     }

//     public function getMinutes() as Lang.Number {
//         return self.minutes;
//     }

//     public function getSeconds() as Lang.Number {
//         return self.seconds;
//     }

//     public function getDurationAsSeconds() as Lang.Number {
//         return self.minutes * 60 + self.seconds;
//     }

//     public function getDurationAsMinutes() as Lang.Number {
//         return self.minutes + self.seconds / 60;
//     }
// }

// class PomodoroTimer {
//     private var workDuration as PomodoroDuration;
//     private var breakDuration as PomodoroDuration;
//     private var timer as Timer.Timer;
//     private var state as PomodoroState;
//     private var paused as Lang.Boolean;
//     private var startTime as Time.Moment;
//     private var endTime as Time.Moment;
//     private var remainingDurationSeconds as Lang.Number;

//     public function initialize(workDuration as PomodoroDuration, breakDuration as PomodoroDuration) {
//         self.workDuration = workDuration;
//         self.breakDuration = breakDuration;

//         self.timer = new Timer.Timer();

//         self.state = $.POST_BREAK;
//         self.paused = true;
//         self.remainingDurationSeconds = 0;
//     }

//     public function saveState() {
//         Application.Storage.setValue("remaining_duration_seconds", endTime.compare(startTime));
//         Application.Storage.setValue("state", self.state);
//         Application.Storage.setValue("paused", self.paused);
//     }

//     public function restoreState() {
//         var remainingDurationSeconds = Application.Storage.getValue("remaining_duration_seconds");
//         var state = Application.Storage.getValue("state");
//         var paused = Application.Storage.getValue("paused");

//         if (remainingDurationSeconds != null) {
//             self.remainingDurationSeconds = remainingDurationSeconds;
//         } else if (state != null) {
//             self.state = state;
//         } else if (paused != null) {
//             self.paused = paused;
//         }

//         WatchUi.requestUpdate();
//     }

//     private function getDefaultState() {
//         return $.POST_BREAK;
//     }

//     public function getDefaultPaused() {
//         return true;
//     }

//     public function start() {
//         if (self.state == $.POST_WORK) {
//             self.startBreakCycle();
//         } else if (self.state == $.POST_BREAK) {
//             self.startWorkCycle();
//         }

//         self.paused = false;
//         self.timer.start(method(:onTimerTick), 1000, true);
//     }

//     public function stop() {
//         self.timer.stop();
//         self.paused = true;
//     }

//     public function reset() {
//         self.timer.stop();
//         self.state = $.POST_BREAK;
//         self.paused = true;
//     }

//     private function startCycle() {
//         self.startTime = Time.now();
//         self.state = getNextCycleState();
//     }

//     private function startWorkCycle() {
//         self.startCycle();
//         self.endTime = new Time.Moment(self.startTime.value() + self.workDuration.getDurationAsSeconds());
//     }

//     private function startBreakCycle() {
//         self.startCycle();
//         self.endTime = new Time.Moment(self.startTime.value() + self.breakDuration.getDurationAsSeconds());
//     }

//     private function stopCycle() {
//         self.timer.stop();
//         self.state = self.getNextCycleState();
//         self.paused = true;

//         alertCycleEnd();
//     }

//     public function onTimerTick() {
//         var currentTime = Time.now();
//         var timeRemaining = self.endTime.compare(currentTime);

//         if (timeRemaining > 0) {
//             self.stopCycle();
//         }

//         WatchUi.requestUpdate();
//     }

//     private function getNextCycleState() {
//         return PomodoroStateByIndex[(self.currentState + 1) % PomodoroStateByIndex.length];
//     }

//     private function alertCycleEnd() as Void {
//         Attention.vibrate([
//             new Attention.VibeProfile(75, 200),
//             new Attention.VibeProfile(0, 100),
//             new Attention.VibeProfile(75, 200),
//             new Attention.VibeProfile(0, 400),
//             new Attention.VibeProfile(75, 200),
//             new Attention.VibeProfile(0, 100),
//             new Attention.VibeProfile(75, 200)
//         ]);
//     }

//     private function alertCycleAction() as Void {
//         Attention.vibrate([
//             new Attention.VibeProfile(75, 200)
//         ]);
//     }
// }